package Week2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.locators.RelativeLocator;


public class RelativeLoc {

	public static void main(String[] args)
	{
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/sorttable.html");
	//	driver.manage().window().maximize();
		
		List<WebElement> textbox = driver.findElements(RelativeLocator.withTagName("td").toRightOf(By.xpath("//td[text()='1001']")));
		for (WebElement webElement : textbox) 
		{
			String text = webElement.getText();
			System.out.println(text);
		}
		
		
		
		
		
		
	}

}